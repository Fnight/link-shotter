package ru.tiiriix.linkshotter.web.rest.dto;

public class OriginalLinkDto {

    private String original;

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }
}
