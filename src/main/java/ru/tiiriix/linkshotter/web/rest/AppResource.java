package ru.tiiriix.linkshotter.web.rest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AppResource {

    @GetMapping(value = "/test")
    public String index(Model model) {
        model.addAttribute("name", "tes");
        return "hello world";
    }

}