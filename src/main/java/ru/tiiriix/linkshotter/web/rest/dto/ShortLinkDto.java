package ru.tiiriix.linkshotter.web.rest.dto;

public class ShortLinkDto {

    private String link;

    public ShortLinkDto(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
