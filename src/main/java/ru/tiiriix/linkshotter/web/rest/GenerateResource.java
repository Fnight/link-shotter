package ru.tiiriix.linkshotter.web.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.tiiriix.linkshotter.service.ShotterService;
import ru.tiiriix.linkshotter.web.rest.dto.OriginalLinkDto;
import ru.tiiriix.linkshotter.web.rest.dto.ShortLinkDto;

@RestController
@RequestMapping(value = "/generate")
public class GenerateResource {

    @Autowired
    private ShotterService shotterService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public ShortLinkDto generateLink(@RequestBody OriginalLinkDto link) {
        return shotterService.makeShortLink(link);
    }

}
