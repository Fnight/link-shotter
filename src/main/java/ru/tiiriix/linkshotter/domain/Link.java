package ru.tiiriix.linkshotter.domain;

import ru.tiiriix.linkshotter.web.rest.dto.ShortLinkDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "links")
public class Link {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "short_link")
    private String shortLink;

    @Column(name = "long_link")
    private String longLink;

    private Long count;

    public Link() {
    }

    public Link(String shortLink, String longLink) {
        this.shortLink = shortLink;
        this.longLink = longLink;
        this.count = 0L;
    }

    public ShortLinkDto getShortLinkDto() {
        return new ShortLinkDto(shortLink);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShortLink() {
        return shortLink;
    }

    public void setShortLink(String shortLink) {
        this.shortLink = shortLink;
    }

    public String getLongLink() {
        return longLink;
    }

    public void setLongLink(String longLink) {
        this.longLink = longLink;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
