package ru.tiiriix.linkshotter.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tiiriix.linkshotter.domain.Link;

import javax.transaction.Transactional;

@Transactional
public interface LinkRepository extends JpaRepository<Link, Long> {

    public Iterable<Link> getLinksById(final Long id);

    public Iterable<Link> findLinksByLongLink(final String longLink);

    public Iterable<Link> findLinksByShortLink(final String shortLink);

}
